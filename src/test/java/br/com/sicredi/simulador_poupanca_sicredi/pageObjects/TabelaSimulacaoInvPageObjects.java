package br.com.sicredi.simulador_poupanca_sicredi.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TabelaSimulacaoInvPageObjects {
	WebDriver driver;
	
	public TabelaSimulacaoInvPageObjects(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > a")
	public WebElement refazerSimulacaoButton;
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > span.texto")
	public WebElement tempoVoceTeraGuardadoLabel;
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > span.valor")
	public WebElement valorVoceTeraGuardadoLabel;
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > div.maisOpcoes")
	public WebElement tabelaOutrasOpcoes;
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > div.maisOpcoes > table > tbody > tr:nth-child(1) > td:nth-child(1)")
	public WebElement mais12mesesLabel;
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > div.maisOpcoes > table > tbody > tr:nth-child(2) > td:nth-child(1)")
	public WebElement mais24mesesLabel;
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > div.maisOpcoes > table > tbody > tr:nth-child(3) > td:nth-child(1)")
	public WebElement mais36mesesLabel;
	
	@FindBy(css = "body > div.conteudoGeral.conteudoGeralCompleto.clearfix > div > div > div.formularioBloco.formularioBlocoResultado > div > div.blocoResultadoSimulacao > div.maisOpcoes > table > tbody > tr:nth-child(4) > td:nth-child(1)")
	public WebElement mais48mesesLabel;
	
}
