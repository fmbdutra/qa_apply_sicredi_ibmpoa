package br.com.sicredi.simulador_poupanca_sicredi.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormularioSimulacaoInvPoupPageObjects {
	WebDriver driver;
	
	public FormularioSimulacaoInvPoupPageObjects(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "#formInvestimento > div:nth-child(1) > input[type=radio]:nth-child(2)")
	public WebElement paraVocePerfilButton;
	
	@FindBy(css = "#formInvestimento > div:nth-child(1) > input[type=radio]:nth-child(4)")
	public WebElement paraEmpresaPerfilButton;
	
	@FindBy(css = "#valorAplicar")
	public WebElement valorAAplicarNumberField;
	
	@FindBy(css = "#valorInvestir")
	public WebElement valorAPouparTodoMesNumberField;
	
	@FindBy(css = "#tempo")
	public WebElement porQuantoTempoPouparNumberField;	
	
	@FindBy(css = "#formInvestimento > div:nth-child(4) > div.blocoInputs.clearfix > div.blocoFormulario.blocoMeses.blocoSelect > a")
	public WebElement porQuantoTempoPouparDropDown;
	
	@FindBy(css = "#formInvestimento > div.simuladorOpcoes.clearfix > ul > li.simular > button")
	public WebElement simularButton;
	
	@FindBy(css = "#formInvestimento > div.simuladorOpcoes.clearfix > ul > li:nth-child(1) > a")
	public WebElement limparFormulario;	
	
	@FindBy(css = "#valorAplicar-error")
	public WebElement aplicaValorError;
	
	@FindBy(css = "#valorInvestir-error")
	public WebElement pouparValorError;
	
	@FindBy(css = "#tempo-error")
	public WebElement tempoValorError;
	
}
