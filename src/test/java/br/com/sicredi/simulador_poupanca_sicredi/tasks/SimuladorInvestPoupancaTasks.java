package br.com.sicredi.simulador_poupanca_sicredi.tasks;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import br.com.sicredi.simulador_poupanca_sicredi.pageObjects.FormularioSimulacaoInvPoupPageObjects;

public class SimuladorInvestPoupancaTasks {
	
	public FormularioSimulacaoInvPoupPageObjects simuladorElements;
	
	public SimuladorInvestPoupancaTasks(WebDriver driver) {
		this.simuladorElements = PageFactory.initElements(driver, FormularioSimulacaoInvPoupPageObjects.class);
	}
	
	public void selecionarPerfilVoce() {
		simuladorElements.paraVocePerfilButton.click();
	}
	
	public void selecionarPerfilEmpresa() {
		simuladorElements.paraEmpresaPerfilButton.click();
	}
	
	public void quantoAplicar(String valor) {
		simuladorElements.valorAAplicarNumberField.sendKeys(valor);
	}
	
	public void quantoPouparTodoMes(String valor) {
		simuladorElements.valorAPouparTodoMesNumberField.sendKeys(valor);
	}
	
	private void porQuantoTempoPoupar(String valor) {
		simuladorElements.porQuantoTempoPouparNumberField.sendKeys(valor);
	}
	
	private void selectTempoAPoupar(String anoMes) {
		if (anoMes == "meses") {
			simuladorElements.porQuantoTempoPouparDropDown.sendKeys(Keys.ARROW_DOWN);
		}
		else {
			simuladorElements.porQuantoTempoPouparDropDown.sendKeys(Keys.ARROW_DOWN);
			simuladorElements.porQuantoTempoPouparDropDown.sendKeys(Keys.ARROW_DOWN);
		}
		
	}
	
	public void porQuantoTempoPouparEmMeses(String valor){
		porQuantoTempoPoupar(valor);
		selectTempoAPoupar("meses");		
	}
	
	public void porQuantoTempoPouparEmAnos(String valor){
		porQuantoTempoPoupar(valor);
		selectTempoAPoupar("anos");		
	}
	
	public void simularInvestimento() {
		simuladorElements.simularButton.click();
	}	
}
