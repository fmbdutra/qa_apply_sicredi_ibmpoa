package br.com.sicredi.simulador_poupanca_sicredi.testCasesAPI;

import org.junit.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class SimuladorInvestPoupancaAPITestCase {

	@Test
	public void testeSimuladorPoupancaGetDeExemplo() {
		given().
		when().
			get("http://5b847b30db24a100142dce1b.mockapi.io/api/v1/simulador").
		then().
			assertThat().
			statusCode(200).
			body("id", equalTo(1)).
			body("meses[0]", equalTo("112")).
			body("meses[1]", equalTo("124")).
			body("meses[2]", equalTo("136")).
			body("meses[3]", equalTo("148")).
			body("valor[0]", equalTo("2.802")).			
			body("valor[1]", equalTo("3.174")).
			body("valor[2]", equalTo("3.564")).
			body("valor[3]", equalTo("3.971"));			
	}
}
