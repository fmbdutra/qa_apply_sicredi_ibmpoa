# qa_apply_sicredi_ibmpoa

Repositório destinado aos testes automatizados de API e UI para o processo seletivo da Sicredi solicitado pela IBM Brasil Porto Alegre

Testes escritos na linguagem **Java**.

Frameworks, técnicas e ferramentas utilizadas:
*  Selenium para testes de UI
*  RestAssured para testes de API
*  Maven para dependências
*  Eclipse IDE
*  PageObjects para organização dos elementos web dos teste de UI

Nos arquivos do projeto, está incluso o chromeDriver para o Google Chrome versão 77~.**<br>
Feito também lógica para verificar se o sistema operacional é Windows ou Linux, para identificar qual arquivos do driver utilizar.

** Não utilizado um webdriver manager (exemplo: https://github.com/bonigarcia/webdrivermanager) pois pela minhas experiências sempre houve problemas no download das dependências do Maven em redes corporativas. Por isso, arquivos do webdriver já inclusos na pasta "chromedriver".

Fabiano Matheus Bittencourt Dutra<br>
09 de Outubro de 2019<br>
Porto Alegre, RS - Brasil


